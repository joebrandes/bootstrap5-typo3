<p align="center">
  <a href="https://getbootstrap.com/">
    <img src="Bootstrap.png" alt="Bootstrap logo" width="200" height="200">
  </a>
</p>

<h3 align="center">Bootstrap 5 Developments</h3>

<p align="center">
  Sleek, intuitive, and powerful front-end framework for faster and easier web development.
  <br>
  <a href="https://getbootstrap.com/docs/5.0/"><strong>Explore Bootstrap docs »</strong></a>

</p>

## Use this Development Environment

- Clone via https://gitlab.com/joebrandes/bootstrap5-typo3.git
- `npm install` - meets necessary sw criteria (node_modules)
- `npm run css` - the easy way!

- This is Building the folder `./dist/css`

  Simple CSS building and watching via Dart Sass

- Use Live Server by ritwickdey or refresh manually

## Bootstrap 5 Developments

For Testing, Developing and Teaching of Bootstrap 5 stuff

Initiated during TYPO3 Dev Cycles and Use for Fluid Templating...

Integrated Development Environment:

- Simple npm Setup for Bootstrap 5, Bootstrap Icons (implemented via npm)

- Sass with Dart Sass (implemented via npm)

  (Alternative: SASS Compiling with VS Code Extension Live Sass Compiler)

- Live Preview with Live Server

Stylings:

- Basic Bootstrap 5 Example Carousel - Folder: docs/carousel
- Basic Boostrap Login Example - Folder: docs/bs5easy
- Bootswatch Themes (just change Main CSS in index.html) - Folder: docs/joeswatch

<hr>

## Bootstrap - Documentation

Bootstrap's documentation, included in this repo in the root directory, is built with [Hugo](https://gohugo.io/) and publicly hosted on GitHub Pages at <https://getbootstrap.com/>. The docs may also be run locally.


### Bootstrap - Documentation for previous releases

You can find all our previous releases docs on <https://getbootstrap.com/docs/versions/>.

[Previous releases](https://github.com/twbs/bootstrap/releases) and their documentation are also available for download.



## Bootstrap - Community

Get updates on Bootstrap's development and chat with the project maintainers and community members.

- Follow [@getbootstrap on Twitter](https://twitter.com/getbootstrap).
- Read and subscribe to [The Official Bootstrap Blog](https://blog.getbootstrap.com/).
- Join [the official Slack room](https://bootstrap-slack.herokuapp.com/).
- Chat with fellow Bootstrappers in IRC. On the `irc.freenode.net` server, in the `##bootstrap` channel.
- Implementation help may be found at Stack Overflow (tagged [`bootstrap-5`](https://stackoverflow.com/questions/tagged/bootstrap-5)).
- Developers should use the keyword `bootstrap` on packages which modify or add to the functionality of Bootstrap when distributing through [npm](https://www.npmjs.com/browse/keyword/bootstrap) or similar delivery mechanisms for maximum discoverability.


## Bootstrap - Versioning

For transparency into our release cycle and in striving to maintain backward compatibility, Bootstrap is maintained under [the Semantic Versioning guidelines](https://semver.org/). Sometimes we screw up, but we adhere to those rules whenever possible.

See [the Releases section of our GitHub project](https://github.com/twbs/bootstrap/releases) for changelogs for each release version of Bootstrap. Release announcement posts on [the official Bootstrap blog](https://blog.getbootstrap.com/) contain summaries of the most noteworthy changes made in each release.


## Bootstrap - Creators

**Mark Otto**

- <https://twitter.com/mdo>
- <https://github.com/mdo>

**Jacob Thornton**

- <https://twitter.com/fat>
- <https://github.com/fat>


## Bootstrap - Copyright and license

Code and documentation copyright 2011–2021 the [Bootstrap Authors](https://github.com/twbs/bootstrap/graphs/contributors) and [Twitter, Inc.](https://twitter.com) Code released under the [MIT License](https://github.com/twbs/bootstrap/blob/main/LICENSE). Docs released under [Creative Commons](https://creativecommons.org/licenses/by/3.0/).

## This Repo

Licensed under [GPLv3](https://gitlab.com/joebrandes/bootstrap5-typo3/blob/main/LICENSE)
